import chai from 'chai'
chai.should()


class Cell {
	constructor({x, y}) {
		this.x = x
		this.y = y
	}

	getCell() {
		return []
	}
}

class LivingCell extends Cell {
	constructor(...arg) {
		super(...arg)
		this._type = 'living'
	}

	getCell() {
		return [
			this
		]
	}

	get prout() {
		return [
			killCell.bind(this),
			killCell.bind(this),
			birthCell.bind(this),
			birthCell.bind(this),
			killCell.bind(this),
			killCell.bind(this),
			killCell.bind(this),
			killCell.bind(this),
			killCell.bind(this),
		]
	}
}

class DeadCell extends Cell {
	constructor(...arg) {
		super(...arg)
		this._type = 'dead'
	}

	getCell() {
		return []
	}

	get prout() {
		return [
			killCell.bind(this),
			killCell.bind(this),
			birthCell.bind(this),
			birthCell.bind(this),
			killCell.bind(this),
			killCell.bind(this),
			killCell.bind(this),
			killCell.bind(this),
			killCell.bind(this),
		]
	}
}

const prout = {} [
	killCell,
	killCell,
	liveCell,
]

function countNeighbour(grid, cell) {
	return grid.reduce((newGrid, cell) => newGrid.concat(cell.getCell()), []).length
}

function nextTurn(grid) {
	return grid.map(cell => {
		const neighbour = countNeighbour(grid, cell)
		return cell.prout[neighbour]()
	})
	
	if (grid.length > 2) {
		return [
			new LivingCell({ x: 0, y: 0 }),
			new LivingCell({ x: 1, y: 0 }),
			new LivingCell({ x: 0, y: 1 }),
			new LivingCell({ x: 1, y: 1 }),
		]
	}
	return [
		new DeadCell(grid[0])
	]
}

describe('Game of life', () => {
	it('1 living cell will die', () => {
		const grid = [
			new LivingCell({x:0, y:0}),
		]
		nextTurn(grid).should.be.deep.equal([
			new DeadCell({x:0, y:0}),
		])
	});

	it('4 living should stay alive', () => {
		const grid = [
			new LivingCell({ x: 0, y: 0 }),
			new LivingCell({ x: 1, y: 0 }),
			new LivingCell({ x: 0, y: 1 }),
			new LivingCell({ x: 1, y: 1 }),
		]
		nextTurn(grid).should.be.deep.equal([
			new LivingCell({ x: 0, y: 0 }),
			new LivingCell({ x: 1, y: 0 }),
			new LivingCell({ x: 0, y: 1 }),
			new LivingCell({ x: 1, y: 1 }),
		])
	})
});
